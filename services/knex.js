import Knex from "knex";
require('dotenv').config();
const knex = Knex({
    client: "pg",
    connection: {
        host: process.env.DATABASE_HOST,
        database: process.env.DATABASE_NAME,
        user: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASS,
        port:process.env.DATABASE_PORT,
        secretKey:process.env.JWT_SECRET
    }
});

export default knex;
