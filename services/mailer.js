import nodemailer from 'nodemailer'
import mailOption from '../templates/forgotPassword.email'

const send = async (mailOptions) => {
    const transporter = nodemailer.createTransport({
        service: process.env.SMTP_SERVICE,
        secure: true,
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASSWORD
        }
    });
    await transporter.sendMail(mailOptions);

};



export const sendForgotPassword = (to, token) =>
    send(mailOption(to, token));
