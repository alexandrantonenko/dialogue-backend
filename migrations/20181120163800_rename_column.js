exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.renameColumn('dialogs_id','dialog_id')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropColumn('dialog_id')
        })
    ])
};
