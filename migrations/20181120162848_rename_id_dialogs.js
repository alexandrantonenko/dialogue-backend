
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.renameColumn('id_dialogs','dialogs_id')
            t.renameColumn('id_client','operator_id')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropColumn('id_client')
        })
    ])
};