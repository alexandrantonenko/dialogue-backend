

exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.boolean('operator_sent')
        })
    ])
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropColumn('operator_sent')
        })
    ])
};
