
exports.up = function(knex, Promise) {
  return Promise.all([
      knex.schema.table('persons', function (t) {
          t.string('restoration_token')
      })
  ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('persons', function (t) {
            t.dropColumn('restoration_token')
        })
    ])
};
