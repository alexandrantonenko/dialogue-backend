exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.integer('status_id')
        })
    ])
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.dropColumn('status_id')
        })
    ])
};