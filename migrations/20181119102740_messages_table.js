exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema
            .createTable('messages_table', function(t) {
                t.increments('id').unsigned().primary();
                t.timestamp('created_at').notNull().default(knex.fn.now());
                t.timestamp('updated_at').nullable();
                t.integer('id_dialogs');
                t.string('text');
            })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('messages_table')
    ]);
};
