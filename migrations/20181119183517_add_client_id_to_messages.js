

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.integer('id_client')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropColumn('id_client')
        })
    ])
};
