
exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropColumn('operator_id')
            t.foreign('dialog_id').references('dialogs_table.id')
        }),
        knex.schema.table('dialogs_table', function (t) {
            t.foreign('client_id').references('persons.id')
            t.foreign('operator_id').references('persons.id')
            t.foreign('status_id').references('statuses.id')
        })
    ])
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropForeign('dialog_id')
            t.integer('operator_id')
        }),
        knex.schema.table('dialogs_table', function (t) {
            t.dropForeign('client_id')
            t.dropForeign('operator_id')
            t.dropForeign('status_id')
        })
    ])
};