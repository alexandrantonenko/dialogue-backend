
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropColumn('client_name')

        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('messages_table', function (t) {
            t.dropColumn('client_name')
        })
    ])
};