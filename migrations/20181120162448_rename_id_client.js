
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.renameColumn('id_client','client_id')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.dropColumn('id_client')
        })
    ])
};
