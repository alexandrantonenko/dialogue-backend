
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.boolean('is_opened')
            t.boolean('is_save')
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.dropColumn('is_opened')
            t.dropColumn('is_save')
        })
    ])
};
