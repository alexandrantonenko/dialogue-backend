exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema
            .createTable('statuses', function (t) {
                t.increments('id').unsigned().primary();
                t.timestamp('created_at').notNull().default(knex.fn.now());
                t.timestamp('updated_at').nullable();
                t.string('status_name');
            })
    ]);
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('status')
    ]);
};
