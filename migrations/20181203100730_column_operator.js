
exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('persons', function (t) {
            t.boolean('operator')
        })
    ])
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.table('persons', function (t) {
            t.dropColumn('operator')
        })
    ])
};