
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.dropColumn('id_operator')
        })
    ])
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.table('dialogs_table', function (t) {
            t.dropColumn('id_operator')
        })
    ])
};