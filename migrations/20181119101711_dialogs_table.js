
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema
            .createTable('dialogs_table', function(t) {
                t.increments('id').unsigned().primary();
                t.timestamp('created_at').notNull().default(knex.fn.now());
                t.timestamp('updated_at').nullable();
                t.integer('id_client');
                t.integer('id_operator');
            })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('dialogs_table')
    ]);
};