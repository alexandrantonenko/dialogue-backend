import jwt from 'jsonwebtoken'

const router = require("koa-router")();

router.prefix("/tokens");

router.get("/validate", async ctx => {
    const decoded = jwt.verify(ctx.query.token, process.env.JWT_SECRET);
    if (decoded) {
        ctx.body = {success: true};
    } else {
        ctx.throw(401)
    }
});

export default router;
