import knex from "../services/knex";
import Answer from "../models/answer"

const router = require("koa-router")();

router.prefix("/answers");

router.get("/get", async ctx => {
    const currAnswers = await Answer.query(knex);
    const answers = currAnswers.map(item => {
        return {value: item.answer.toLowerCase(), label: item.answer}
    });
    ctx.body = {answers}
});


export default router