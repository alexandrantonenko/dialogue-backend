import knex from "../services/knex";
import Person from "../models/person";

const router = require("koa-router")();

router.prefix("/users");

router.post("/", async ctx => {
    const {id} = ctx.state.user;
    await Person.query(knex).patchAndFetchById(id, ctx.request.body);
    ctx.body = {success: true};
});

router.get("/username", async ctx => {
    const {login} = ctx.query;
    const [user] = await Person.query(knex).where({login});
    if (user) {
        ctx.throw(500);
    } else {
        ctx.body = {success: true};
    }
});

export default router;
