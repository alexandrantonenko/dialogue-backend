import knex from '../services/knex'
import Dialog from '../models/dialog'
import Message from '../models/message'
import Client from '../models/person'
import Status from '../models/status'
import { transaction } from 'objection'
import firebase from 'firebase-admin'
import { STATUSES } from '../utils/const'

var serviceAccount = require('../ns-dialog-firebase-adminsdk-njp6o-ddd64b7558.json')
const router = require('koa-router')()

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
})

router.prefix('/dialog')

router.post('/', async ctx => {
  await transaction(knex, async tx => {
    const status = await Status.query(tx).where({ status_name: STATUSES.NEW })
    const client = await Client.query(tx).where({
      login: ctx.request.body.login,
    })
    const dialog = await Dialog.query(tx).insert({
      client_id: client[0].id,
      status_id: status[0].id,
    })
    await Message.query(tx).insert({
      text: ctx.request.body.text,
      dialog_id: dialog.id,
      operator_sent: false,
    })
    ctx.body = { success: true }
    firebase.messaging().send({
      data: {
        title: 'New dialogue',
        body: 'Dialogue was create',
      },
      topic: 'all',
    })
  })
})

router.post('/complete', async ctx => {
  const status = await Status.query(knex).where({ status_name: STATUSES.CLOSE })
  await Dialog.query(knex)
    .where({ id: ctx.query.id })
    .patch({
      status_id: status[0].id,
      rating: Number(ctx.query.rating),
    })
  ctx.body = { success: true }
})

router.post('/token', async ctx => {
  firebase.messaging().subscribeToTopic(ctx.query.token, 'all')
  ctx.body = { success: true }
})

router.post('/change', async ctx => {
  await transaction(knex, async tx => {
    await Dialog.query(tx)
      .where({ id: ctx.query.id })
      .patch({
        status_id: Number(ctx.query.status),
        operator_id: Number(ctx.query.operator_id) || undefined,
      })
    ctx.body = { success: true }
  })
})

const dialogsNumber = length => length || 0

router.get('/get', async ctx => {
  await transaction(knex, async tx => {
    const status = ctx.query.status
    const dialogs_length = await knex('dialogs_table')
      .count('*')
      .where({ status_id: status })
    const length = ctx.query.length
    const operator_id = ctx.query.operator_id
      ? `= ${ctx.query.operator_id}`
      : 'is null'
    const dialogs = await tx.raw(
      'WITH x AS (\n' +
        'SELECT\n' +
        'messages_table.created_at,\n' +
        'dialogs_table.status_id,\n' +
        'dialogs_table.id,\n' +
        'dialogs_table.operator_id,\n' +
        'persons.login,\n' +
        'messages_table.text,\n' +
        'dialogs_table.rating, \n' +
        'ROW_NUMBER() OVER (PARTITION BY dialogs_table.id ORDER BY messages_table.created_at DESC) priority\n' +
        'FROM dialogs_table\n' +
        '\tJOIN messages_table ON messages_table.dialog_id = dialogs_table.id\n' +
        '\tJOIN persons ON persons.id = dialogs_table.client_id\n' +
        ')\n' +
        `SELECT created_at, id,status_id, login, text, operator_id, rating FROM x WHERE priority = 1 and status_id = ${status} and operator_id ${operator_id}  \n` +
        'ORDER BY created_at DESC \n' +
        `OFFSET ${dialogsNumber(length)} LIMIT 8`
    )
    ctx.body = { ...dialogs, totalCount: Number(dialogs_length[0].count) }
  })
})

router.get('/search', async ctx => {
  await transaction(knex, async tx => {
    const status = ctx.query.status
    const word = decodeURI(ctx.query.word.toLowerCase())
    const dialogs_length = await knex('dialogs_table')
      .count('*')
      .where({ status_id: status })
    const operator_id = ctx.query.operator_id
      ? `= ${ctx.query.operator_id}`
      : 'is null'
    const dialogs = await tx.raw(
      'WITH x AS (\n' +
        'SELECT\n' +
        'messages_table.created_at,\n' +
        'messages_table.id as message_id,\n' +
        'dialogs_table.status_id,\n' +
        'dialogs_table.id,\n' +
        'dialogs_table.operator_id,\n' +
        'persons.login,\n' +
        'messages_table.text,\n' +
        'ROW_NUMBER() OVER (PARTITION BY dialogs_table.id ORDER BY messages_table.created_at DESC)\n' +
        'FROM dialogs_table\n' +
        '\tJOIN messages_table ON messages_table.dialog_id = dialogs_table.id\n' +
        '\tJOIN persons ON persons.id = dialogs_table.client_id\n' +
        `\tWHERE true AND lower(messages_table.text) LIKE '%${word}%' OR lower(persons.login) LIKE '%${word}%' \n` +
        ')\n' +
        `SELECT created_at,message_id, id,status_id, login, text, operator_id FROM x WHERE true and status_id = ${status} and operator_id ${operator_id}`
    )
    ctx.body = { ...dialogs, totalCount: Number(dialogs_length[0].count) }
  })
})

router.get('/delete', async ctx => {
  await Message.query(knex)
    .where({ dialog_id: ctx.query.id })
    .delete()
  await Dialog.query(knex)
    .where({ id: ctx.query.id })
    .delete()
  ctx.body = { success: true }
})

export default router
