import knex from '../services/knex'
import Message from '../models/message'
import Person from '../models/person'
import { transaction } from 'objection'

const router = require('koa-router')()

router.prefix('/messages')

router.post('/send', async ctx => {
  const person = await Person.query(knex).where({
    login: ctx.request.body.login,
  })
  const message = await Message.query(knex).insert({
    dialog_id: ctx.request.body.id,
    text: ctx.request.body.text,
    operator_sent: person[0].operator,
  })
  ctx.body = { message }
})

router.get('/get', async ctx => {
  const time = ctx.query.time || new Date().toLocaleString()
  const message_id = ctx.query.message_id
  const where = message_id
    ? `  id > ${message_id - 20} order by created_at desc`
    : `date_trunc('milliseconds',created_at) < '${time}' order by created_at desc limit 30`
  await transaction(knex, async tx => {
    const message = await tx.raw(
      'SELECT * from \n' +
        `(select * from messages_table WHERE dialog_id = ${
          ctx.query.id
        } AND ${where} ) temp \n` +
        'ORDER BY id ASC'
    )
    ctx.body = { message }
  })
})

router.get('/search', async ctx => {
  await transaction(knex, async tx => {
    const words = decodeURI(ctx.query.words.toLowerCase())
    const messages_id = await tx.raw(
      'SELECT id from \n' +
        `(select messages_table.id, messages_table.text from messages_table WHERE dialog_id = ${
          ctx.query.id
        } AND lower(messages_table.text) LIKE '%${words}%'  order by id DESC) temp \n` +
        'ORDER BY id DESC'
    )
    ctx.body = { messages_id }
  })
})

export default router
