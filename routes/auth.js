import passport from 'koa-passport'
import jwt from 'jsonwebtoken'
import knex from "../services/knex";
import Person from '../models/person';
import {transaction} from 'objection';
import {sendForgotPassword} from '../services/mailer'
//  fuser -k 3000/tcp
const router = require('koa-router')()

router.prefix("/auth");

const wrapUserWithToken = (ctx, user, google, vkontakte) => {
    if (user) {
        const token = jwt.sign(user.$toJson(), process.env.JWT_SECRET);
        ctx.body = {user, token, google: !!google, vkontakte: !!vkontakte};
    } else {
        ctx.throw(401);
    }
};


router.post("/sign_up", async ctx => {
    await transaction(knex, async tx => {
        const user = await Person.query(tx).insert({
            ...ctx.request.body,
        });
        wrapUserWithToken(ctx, user)
    })
});

router.post("/sign_in", async ctx => {
    await passport.authenticate("local", {session: false}, async (err, user) => {
        wrapUserWithToken(ctx, user)
    })(ctx);
});

// router.get("/google", passport.authenticate('google', {
//         scope: [
//             'https://www.googleapis.com/auth/plus.login',
//             'https://www.googleapis.com/auth/plus.profile.emails.read'
//         ]}
//     )
//     );
//
// router.get('/google/callback', async ctx => {
//     await passport.authenticate('google', { failureRedirect: '/' , successRedirect: '/'}, async (err,user)=>{
//     wrapUserWithToken(ctx,user,true)})(ctx)
//     });

router.get('/google', async ctx => {
    await passport.authenticate('google-token', {session: false}, async (err, user) => {
        wrapUserWithToken(ctx, user, true)
    })(ctx)
});

// router.get('/vkontakte', passport.authenticate ('vkontakte'))
//
// router.get('/vkontakte/callback', async ctx =>{
//     await passport.authenticate('vkontakte', {scope: ['email']} ,async (err,user)=>{
//         wrapUserWithToken(ctx,user,false,true)
//     })(ctx)
// })

router.get('/vkontakte', async ctx => {
    await passport.authenticate('vkontakte-token', {session: false}, async (err, user) => {
        wrapUserWithToken(ctx, user, false, true)
    })(ctx)
});

router.get('/forgot_password', async ctx => {
    const restoration_token = await Person.getRandomToken();
    const login = ctx.request.query.login
    const person = await Person.query(knex)
        .patch({restoration_token})
        .where({login});

    if (person) {
        await sendForgotPassword(ctx.request.query.login, restoration_token);
        ctx.body = {success: true};
    } else {
        ctx.throw(500)
    }
});


router.post("/update_password", async ctx => {
    await Person.query(knex)
        .patch({
            password: ctx.request.body.password,
            restoration_token: null
        })
        .where({restoration_token: ctx.request.query.token});

    ctx.body = {success: true};
});


router.get('/logout', function (ctx) {
    // req.logout();
    // req.session = null;
})

module.exports = router;