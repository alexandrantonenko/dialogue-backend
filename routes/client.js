import knex from "../services/knex";
import {transaction} from 'objection';
import Person from "../models/person";

const router = require('koa-router')();

router.prefix("/client");

router.post("/sign_up", async ctx => {
    await transaction(knex, async tx => {
        await Person.query(tx).insert({
            ...ctx.request.body, operator: false
        });
    })
});

module.exports = router;