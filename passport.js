import passport from 'koa-passport'
import {Strategy as LocalStrategy} from 'passport-local'
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt'
import { Strategy as GoogleTokenStrategy } from 'passport-google-token'
import VkontakteTokenStrategy from 'passport-vkontakte-token'
import Person from './models/person'
import knex from './services/knex'
import { raw, transaction } from 'objection';

// const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

passport.serializeUser((user,done)=>{
    done(null,user.id);
});

passport.use(new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password',
    session: false,
}, async (login, password, done) => {
    const [user] = await Person
        .query(knex)
        .where(raw('lower(login)'), '=', login.toLowerCase())
    if (user && user.verifyPassword(password)) {
        done(null, user);
    } else {
        done(null, false);
    }
}));

passport.use(new JWTStrategy(
    {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET,
    },
    (async ({ id }, done) => {
        const [user] = await Person
            .query(knex)
            .where('id', '=', id);
        if (user) {
            return done(null, user);
        }
        return done(null, false);
    }),
));

passport.use(new VkontakteTokenStrategy({
    clientID: process.env.VKONTAKTE_APP_ID,
    clientSecret: process.env.VKONTAKTE_APP_SECRET,
    passReqToCallback: true
}, async (accessToken, refreshToken, params,profile, done) => {
        const person = await transaction (knex,async(tx) => {
            const login = profile.id.toString()
            const [existingPerson] = await Person
                .query(tx)
                .where('login','=',login)
            if(existingPerson){
                return existingPerson
            }
            return Person
                .query(tx)
                .insert({
                    login,
                    password: new Date().toString()
                })
        })
    if(person) {
        return done(null, person)
    }
    return done(null,false)
    }
))


passport.use(new GoogleTokenStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
}, async (accessToken,refreshToken,{_json: {email} },done) => {
    const person = await transaction(knex, async (tx) => {
        const login = email
        const [existingPerson] = await Person
            .query(tx)
            .where('login','=',login);
        if (existingPerson){
            return existingPerson
        }
        return Person
            .query(tx)
            .insert({
                login,
                password: new Date().toString(),
            })
    } )
    if (person) {
        return done(null, person)
    }
    return done(null, false)
    }
))

passport.deserializeUser((id, done) => {
    Person.findById(id, (err, user) => {
        console.log('Найден ', user.login)
        done(err, user.login);
    })
});
