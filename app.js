import Koa from 'koa'
import cors from 'koa2-cors'
import json from 'koa-json'
import onerror from 'koa-onerror'
import bodyparser from 'koa-bodyparser'
import logger from 'koa-logger'
import unless from 'koa-unless';
import users from './routes/users'
import auth from './routes/auth'
import dialog from './routes/dialog'
import tokens from './routes/tokens'
import client from './routes/client'
import answers from './routes/answers'
import messages from './routes/messages'
import passport from 'koa-passport'
import './passport'

const app = new Koa();

const passportJwt = passport.authenticate('jwt', {session: false});
passportJwt.unless = unless;

// error handler
onerror(app);


// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(passportJwt.unless({
    path: [/^\/auth\//, /^\/users\/username/, /^\/tokens\//, /^\/dialog\//, /^\/client\//, /^\/answers\//, /^\/messages\//],
}));

app.use(json());
app.use(logger());
app.use(cors());

// logger
app.use(async (ctx, next) => {
    const start = new Date();
    await next();
    const ms = new Date() - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
});

// routes
app.use(users.routes(), users.allowedMethods());
app.use(auth.routes(), auth.allowedMethods());
app.use(tokens.routes(), tokens.allowedMethods());
app.use(dialog.routes(), dialog.allowedMethods());
app.use(client.routes(), client.allowedMethods());
app.use(answers.routes(), answers.allowedMethods());
app.use(messages.routes(), messages.allowedMethods());

// error-handling
app.on('error', (err, ctx) => {
    console.error('server error', err, ctx)
});

module.exports = app;
