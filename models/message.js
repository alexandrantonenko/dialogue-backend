import BaseModel from "./BaseModel";
import Dialog from "./dialog"
import {Model} from "objection";

export default class Message extends BaseModel {
    static get tableName() {
        return "messages_table";
    }

    static get pickJsonSchemaProperties() {
        return true;
    }

    static get jsonSchema() {
        return {
            type: "object",
            required: ["dialog_id"],
            properties: {
                id: {type: "integer"},
                dialog_id: {type: "integer"},
                text: {type: "string"},
                operator_id: {type: "integer"},
                operator_sent: {type: "boolean"}
            }
        };
    }

}