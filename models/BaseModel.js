import { Model } from "objection";

export default class BaseModel extends Model {
    $beforeInsert() {
        this.created_at = new Date();
    }

    $beforeUpdate() {
        this.updated_at = new Date();
    }

    static get idColumn() {
        return "id";
    }
}
