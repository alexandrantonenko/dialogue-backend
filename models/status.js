import BaseModel from "./BaseModel";
import Dialog from "./dialog";
import {Model} from "objection";

export default class Status extends BaseModel {
    static get tableName() {
        return "statuses";
    }

    static get pickJsonSchemaProperties() {
        return true;
    }

    static get jsonSchema() {
        return {
            type: "object",
            properties: {
                id: {type: "integer"},
                status_name: {type: "string"},
            }
        };
    }

    static get relationMappings() {
        return {
            status: {
                relation: Model.HasManyRelation,
                modelClass: Dialog,
                join: {
                    from: "statuses.id",
                    to: "dialogs_table.status_id"
                }
            },

        }
    };
}