import bcrypt from "bcrypt";
import BaseModel from "./BaseModel";
import {Model} from "objection";
import Dialog from "./dialog"
import Message from "./message"

const password = require("objection-password")();

export default class Person extends password(BaseModel) {
    static get tableName() {
        return "persons";
    }

    static getRandomToken() {
        return bcrypt.hash(new Date().valueOf().toString(), 10);
    }

    $formatJson(json) {
        const formattedJson = super.$formatJson(json);
        delete formattedJson.password;
        delete formattedJson.restoration_token;
        return formattedJson;

    }

    static get pickJsonSchemaProperties() {
        return true;
    }

    static get jsonSchema() {
        return {
            type: "object",
            required: ["login", "password"],

            properties: {
                id: {type: "integer"},
                login: {type: "string"},
                password: {type: "string"},
                restoration_token: {type: ["string", "null"]},
                operator: {type: "boolean"}
            }
        };
    }

    static get relationMappings() {
        return {
            client: {
                relation: Model.HasManyRelation,
                modelClass: Dialog,
                join: {
                    from: "persons.id",
                    to: "dialogs_table.client_id"
                }
            },
            operator: {
                relation: Model.HasManyRelation,
                modelClass: Dialog,
                join: {
                    from: "persons.id",
                    to: "dialogs_table.operator_id"
                }
            }
        }
    };
}

Person.prototype.verifyPassword = function verifyPassword(pwd) {
    return bcrypt.compareSync(pwd, this.password);
};
