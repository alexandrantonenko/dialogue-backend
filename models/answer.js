import BaseModel from "./BaseModel";

export default class Answer extends BaseModel {
    static get tableName() {
        return "answers";
    }

    static get pickJsonSchemaProperties() {
        return true;
    }

    static get jsonSchema() {
        return {
            type: "object",
            properties: {
                id: {type: "integer"},
                answer: {type: "string"}
            }
        };
    }
}