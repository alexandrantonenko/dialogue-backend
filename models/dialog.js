import BaseModel from "./BaseModel";
import Person from "./person"
import Message from "./message"
import {Model} from "objection";

export default class Dialog extends BaseModel {
    static get tableName() {
        return "dialogs_table";
    }

    static get pickJsonSchemaProperties() {
        return true;
    }

    static get jsonSchema() {
        return {
            type: "object",
            properties: {
                id: {type: "integer"},
                client_id: {type: "integer"},
                client_name:{type:"string"},
                status_id: {type: "integer"},
                operator_id: {type: "integer"},
                rating: {type: "integer"}
            }
        };
    }

    static get relationMappings() {
        return {
            messages: {
                relation: Model.HasManyRelation,
                modelClass: Message,
                join: {
                    from: "dialogs_table.id",
                    to: "messages_table.dialog_id"
                }
            },
            clients: {
                relation: Model.BelongsToOneRelation,
                modelClass: Person,
                join: {
                    from: "persons.id",
                    to: "dialogs_table.client_id"
                }
            }

        };
    }

}
