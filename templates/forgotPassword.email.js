const mailOptions = (to, token) => ({
    from: process.env.SMTP_FROM,
    to,
    subject: "Привет",
    text: `Ссылка для восстановления пароля - ${
        process.env.WEB_APP_URL
        }/update_password?token=${token}`,
    html: `<b>Ссылка для восстановления пароля - <a href="${
        process.env.WEB_APP_URL
        }/update_password?token=${token}">перейти</a></b>`
});

export default mailOptions;
