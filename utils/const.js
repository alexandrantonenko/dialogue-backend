export const NOTIFICATION_TYPES = {
    DIALOGUE_CREATED: "DIALOGUE_CREATED"
}

export const STATUSES = {
    NEW: "new",
    ACTIVE: "active",
    SAVE: "save",
    CLOSE: "close"
}